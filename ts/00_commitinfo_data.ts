/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const name: string = '@pushrocks/smartversion';
export const version: string = '3.0.2';
export const description: string = 'handle semver with ease'
export const commitinfo = {
  name: '@pushrocks/smartversion',
  version: '3.0.2',
  description: 'handle semver with ease'
}
