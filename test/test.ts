import { expect, tap } from '@pushrocks/tapbundle';
import * as smartversion from '../ts/index.js';

tap.test('should create a valid SmartVersion', async () => {
  const localSmartVersion = new smartversion.SmartVersion('3.2.1');
  expect(localSmartVersion).toBeInstanceOf(smartversion.SmartVersion);
  // tslint:disable-next-line:no-unused-expression
  expect(localSmartVersion.greaterThanString('4.0.0')).toBeFalse();
  // tslint:disable-next-line:no-unused-expression
  expect(localSmartVersion.greaterThanString('3.0.0')).toBeTrue();
  // tslint:disable-next-line:no-unused-expression
  expect(localSmartVersion.lessThanString('v4.0.0')).toBeTrue();
  // tslint:disable-next-line:no-unused-expression
  expect(localSmartVersion.lessThanString('v3.0.0')).toBeFalse();
});

tap.test('should create a valid SmartVersion', async () => {
  const localSmartVersion = smartversion.SmartVersion.fromFuzzyString('4');
  expect(localSmartVersion).toBeInstanceOf(smartversion.SmartVersion);
  console.log(localSmartVersion.versionString);
});

tap.test('should create a valid SmartVersion', async () => {
  const localSmartVersion = smartversion.SmartVersion.fromFuzzyString('4.x');
  const bestMatch = localSmartVersion.getBestMatch(['4.0.1', '4.7.5', '4.3.0']);
  console.log(bestMatch);
  expect(bestMatch).toEqual('4.7.5');
});

tap.test('should create a patch version', async () => {
  const versInstance =  smartversion.SmartVersion.fromFuzzyString('1.2.3');
  const newVersion = versInstance.getNewPatchVersion();
  console.log(newVersion.versionString);
})

tap.start();
